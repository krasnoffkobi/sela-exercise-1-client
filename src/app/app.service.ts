import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private _http: Http) {}

  createAuthorizationHeader(headers: Headers) {
    headers.append('Content-Type', 'application/json');
  }

  getMethod(url: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    
    return this._http.get(url, { headers: headers }).pipe(map(res => res.json()))
  }

  postMethod(url: string, body: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);

    return this._http.post(url, body, { headers: headers }).pipe(
        map(res => res.json()));
  }

  deleteMethod(url: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    
    return this._http.delete(url, { headers: headers }).pipe(map(res => res.json()))
  }

  putMethod(url: string, body: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);

    return this._http.put(url, body, { headers: headers }).pipe(
        map(res => res.json()));
  }
}
