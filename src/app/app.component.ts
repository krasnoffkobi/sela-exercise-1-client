import { Component, OnInit, ViewChild } from '@angular/core';
import {AppService} from './app.service';
import { AppConfig } from './app.config';

import { ExampleDataSource } from './example-data-source'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent implements OnInit {
  title = 'sela-client';
  @ViewChild('table')table: any;
  isButtonVisible: boolean;
  
  displayedColumns = ["firstName", "lastName", "phoneNumber", "id"]
  
  cardTitle: string;
  cardButtonTest: string;

  baseHref: any;
  
  readonly nullGuid = "00000000-0000-0000-0000-000000000000";

  filterstr: string;

  postData = {
    id: this.nullGuid,
    firstName: "",
    lastName: "",
    phoneNumber: ""
  };

  constructor(private _httpService:AppService) {}

  ngOnInit() {
    this.getPhoneList();
    this.clearPostData();
    this.setCardToAdd();

    this.baseHref = AppConfig.settings.env;
  }

  onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        /*this.form.get('avatar').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })*/
        this.onSendFile(reader.result.toString());
      };
    }
  }

  onEdit(id: string) {
    this.setCardToEdit();
    this.postData = this.table.dataSource.dataArr.filter(el => el.id === id)[0];
  }

  onCancel() {
    this.clearPostData();
    this.setCardToAdd();
  }

  onSendFile(base64File: string) {
    var payload = {
      "fileData": base64File
    };

    this._httpService.postMethod(AppConfig.settings.env + 'api/phonelist/uploadFile', JSON.stringify(payload))
      .subscribe (
        data => {
          this.getPhoneList();
          this.clearPostData();
        },
        error => {
          alert(error);
        }
      );
  }

  onAddPhone() {
    if (this.postData.id == this.nullGuid)
    {
      this._httpService.postMethod(AppConfig.settings.env + 'api/phonelist/addNewPhone', JSON.stringify(this.postData))
      .subscribe (
        data => {
          this.getPhoneList();
          this.clearPostData();
        },
        error => {
          alert(error);
        }
      );
    }
    else {
      this._httpService.putMethod(AppConfig.settings.env + 'api/phonelist/EditPhone/' + this.postData.id, JSON.stringify(this.postData))
      .subscribe (
        data => {
          this.getPhoneList();
          this.clearPostData();
          this.setCardToAdd();
        },
        error => {
          alert(error);
        }
      );
    }
  }

  onDelete(id: string) {
    this._httpService.deleteMethod(AppConfig.settings.env + 'api/phonelist/deletePhone/' + id)
    .subscribe (
      data => {
        this.getPhoneList();
        
      },
      error => {
        alert(error);
      }
    );
  }

  search(val: any) {
    this.getPhoneList();
  }

  getPhoneList() {
    this._httpService.getMethod(AppConfig.settings.env + 'api/phonelist/getPhoneList')
    .subscribe (
      data => {
        if (this.filterstr) 
          data = data.filter(d => (d.firstName + " " + d.lastName).indexOf(this.filterstr) >= 0);
        
        this.table.dataSource = new ExampleDataSource(data);
      },
      error => {
        alert(error);
      }
    );
  }

  private clearPostData() {
    this.postData = {
      id: this.nullGuid,
      firstName: "",
      lastName: "",
      phoneNumber: ""
    };
  }

  private setCardToAdd() {
    this.cardTitle = "Add New Phone";
    this.cardButtonTest = "Add";
    this.isButtonVisible = false;

  }

  private setCardToEdit() {
    this.cardTitle = "Edit Phone";
    this.cardButtonTest = "Edit";
    this.isButtonVisible = true;
  }
}
